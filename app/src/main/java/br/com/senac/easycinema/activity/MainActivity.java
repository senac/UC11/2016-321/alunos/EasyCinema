package br.com.senac.easycinema.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import br.com.senac.easycinema.R;

public class MainActivity extends AppCompatActivity {

    private FrameLayout conteudo;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            conteudo.removeAllViews();

            switch (item.getItemId()) {
                case R.id.navigation_filme:
                    setConteudoCartaz(); ;
                    return true;
                case R.id.navigation_cinema:
                   setConteudoCinema() ;
                    return true;
                case R.id.navigation_sessao:
                   setConteudoSessao();
                    return true;
            }
            return false;
        }

    };

    private void setConteudoSessao() {
        conteudo.addView(getLayoutInflater().inflate(R.layout.descricao, conteudo , false) );
    }

    private void setConteudoCinema() {
        conteudo.addView(getLayoutInflater().inflate(R.layout.filtro, conteudo , false) );
    }


    private void setConteudoCartaz() {
        conteudo.addView(getLayoutInflater().inflate(R.layout.filmes_cartaz , conteudo , false) );
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        conteudo = (FrameLayout) findViewById(R.id.content);
        this.setConteudoCartaz();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
